import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Event} from "../../models/Event"
import {EventService} from "../../services/EventService";
import {EventDetailPage} from "./eventDetail/eventDetail";
import {Storage} from "@ionic/storage";
import {Network} from "@ionic-native/network";

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage implements OnInit {
  events: any = [];
  key: string = 'events';

  constructor(
    public navCtrl: NavController,
    private eventService: EventService,
    private storage: Storage,
    private network: Network) {
  }

  ngOnInit() {
    this.loadEvents();
  }

  loadEvents() {
    if (this.network.type === 'none') {
      this.loadData();
    } else {
      this.getEvents();
      this.saveData();
    }
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.loadEvents();
      refresher.complete();
    }, 2000);
  }

  goToEventDetail(event: Event) {
    this.navCtrl.push(EventDetailPage, {event: event});
  }

  getEvents() {
    this.eventService
      .fetchData()
      .subscribe(
        events => {
          let regex = JSON.stringify(events).replace(/[._ ]+/g, "");
          this.events = JSON.parse(regex);
        });
  }

  saveData() {
    this.storage.set(this.key, JSON.stringify(this.events));
  }

  loadData() {
    this.storage.get(this.key)
      .then((value => {
          if (value != null && value != undefined) {
            this.events = JSON.parse(value);
          }
        }
      ));
  }
}
