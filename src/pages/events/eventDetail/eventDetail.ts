import {Component, Input, OnInit} from "@angular/core";
import {Event} from "../../../models/Event";
import {NavParams} from "ionic-angular";

@Component({
  selector: 'page-eventDetail',
  templateUrl: 'eventDetail.html'
})

export class EventDetailPage implements OnInit{
  @Input() event: Event;

  constructor(public navParams: NavParams) { }

  ngOnInit() {
    this.event = this.navParams.get('event');
  }
}
