import {Input, Component} from "@angular/core";
import {Event} from "../../../models/Event"
import {faPlane} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'page-flight',
  templateUrl: 'flight.html'
})
export class FlightPage {
  faPlane = faPlane;
  @Input() event: Event;

}
