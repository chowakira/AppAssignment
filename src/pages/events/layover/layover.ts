import {Input, Component} from "@angular/core";
import {Event} from "../../../models/Event"
import {faSuitcase} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'page-layover',
  templateUrl: 'layover.html'
})
export class LayoverPage {
  faSuitcase = faSuitcase;
  @Input() event: Event;

}
