import {Input, Component} from "@angular/core";
import {Event} from "../../../models/Event"
import {faPaste} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'page-standby',
  templateUrl: 'standby.html'
})
export class StandbyPage {
  faPaste = faPaste;
  @Input() event: Event;

}
