export interface Event {
  Flightnr: string;
  Date: string;
  AircraftType: string;
  Tail: number;
  Departure: string;
  Destination: string;
  TimeDepart: string;
  TimeArrive: string;
  DutyID: string;
  DutyCode: string;
  Captain: string;
  FirstOfficer: string;
  FlightAttendant: string;
}

