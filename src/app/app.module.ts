import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from "@ionic/storage";
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {HttpClientModule} from "@angular/common/http";

import {MyApp} from './app.component';
import {EventsPage} from '../pages/events/events';
import {EventService} from "../services/EventService";
import {FlightPage} from "../pages/events/flight/flight";
import {LayoverPage} from "../pages/events/layover/layover";
import {StandbyPage} from "../pages/events/standby/standby";
import {EventDetailPage} from "../pages/events/eventDetail/eventDetail";

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faPaste} from '@fortawesome/free-solid-svg-icons';
import {faSuitcase} from '@fortawesome/free-solid-svg-icons';
import {faPlane} from '@fortawesome/free-solid-svg-icons';
import {Network} from "@ionic-native/network";


// Add an icon to the library for convenient access in other components
library.add(faPaste, faSuitcase, faPlane);

@NgModule({
  declarations: [
    MyApp,
    EventsPage,
    FlightPage,
    LayoverPage,
    StandbyPage,
    EventDetailPage
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    EventsPage,
    EventDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EventService,
    Network
  ],
})
export class AppModule {
}
