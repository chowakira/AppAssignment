import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Event} from "../models/Event";
import "rxjs/add/operator/map";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class EventService {
  url: string = 'https://get.rosterbuster.com/wp-content/uploads/dummy-response.json';

  constructor(private http: HttpClient) {
  }

  fetchData() {
    return this.http
      .get<Observable<Event[]>>(this.url);
    // .map(response => response.json());
  }
}
